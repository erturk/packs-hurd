#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get
from pisi.actionsapi import shelltools
def setup():
    shelltools.export("LDFLAGS","")
    autotools.autoreconf("-fi")
    autotools.configure("--exec-prefix=/ \
		            --disable-net-group \
		            --disable-pcmcia-group \
		            --disable-wireless-group")

def build():
    autotools.make()

def install():
    autotools.rawInstall('DESTDIR="%s" gnumach.gz gnumach gnumach.msgids' % get.installDIR())
   # pisitools.domove("/boot/gnumach","/boot/gnumach-%s" %get.srcVERSION())
    pisitools.dodoc("AUTHORS", "ChangeLog", "DEVELOPMENT", "README")
