# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
def setup():

    shelltools.system("ar xf init-system-helpers_1.47_all.deb")
    shelltools.system("tar xvf data.tar.xz")
    shelltools.system("rm -rf data.tar.xz")
    shelltools.system("ar xf insserv_1.14.0-5.4_hurd-i386.deb")
    shelltools.system("tar xvf data.tar.xz")
    shelltools.system("rm -rf data.tar.xz")
    shelltools.system("ar xf initscripts_2.88dsf-59.8_hurd-i386.deb")
    shelltools.system("tar xvf data.tar.xz")
    shelltools.system("rm -rf data.tar.xz")
    shelltools.system("ar xf init_1.47_hurd-i386.deb")
    shelltools.system("tar xvf data.tar.xz")
    shelltools.system("rm -rf data.tar.xz")


def install():
    for i in ("etc", "lib","sbin", "usr","var"):
         pisitools.insinto("/", i)


