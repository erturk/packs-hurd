#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get
from pisi.actionsapi import shelltools
def setup():
   # shelltools.export("LDFLAGS","")
    autotools.autoreconf("-vfi")
    autotools.rawConfigure("--prefix= \
                         --disable-profile \
                         --without-libbz2 \
                        --without-libz \
                         --enable-static-progs='iso9660fs,ext2fs' \
                         --without-parted")
	
    shelltools.unlinkDir("libdde-linux26/lib/src/arch/l4")
    shelltools.unlinkDir("libdde-linux26/contrib")

def build():
    autotools.make("libihash")
    autotools.make()
    autotools.make("-C libdde-linux26")
    autotools.make("-C libmachdev")
    autotools.make("-C libddekit")


def install():
    autotools.install('DESTDIR="%s SBINDIR=/sbin BINDIR=/bin "' % get.installDIR())
    autotools.rawInstall('-C libdde-linux26 DESTDIR="%s"' % get.installDIR())

    pisitools.domove("/usr/hurd/*" ,"/hurd/")

    pisitools.domove("/usr/dev/*" ,"/dev/")
    pisitools.domove("/usr/libexec/*" ,"/sbin/")

    pisitools.domove("/usr/sbin/*" ,"/sbin/")
    pisitools.domove("/usr/bin/*" ,"/bin/")

  #  for i in ["sbin/runsystem", "sbin/rc", "sbin/runsystem.hurd"]:
    #    pisitools.domove(i, "/etc/hurd/")


    pisitools.rename("/bin/ps", "ps-hurd") 
    pisitools.rename("/sbin/halt", "halt-hurd")
    pisitools.rename("/bin/login", "login-hurd")
    pisitools.rename("/sbin/fsck", "fsck-hurd")
    pisitools.rename("/sbin/reboot", "reboot-hurd")
    pisitools.rename("/sbin/rc", "rc-hurd")



   # pisitools.insinto("/usr/lib", "libdde-linux26/lib/src/libdde_*.a")

    pisitools.insinto("/usr/share/libdde_linux26", "libdde-linux26/Makeconf*")
    pisitools.insinto("/usr/share/libdde_linux26/mk", "libdde-linux26/mk/*")
    pisitools.insinto("/usr/share/libdde_linux26/build", "libdde-linux26/build/*")
    pisitools.insinto("/usr/include", "libddekit/ddekit")
    pisitools.insinto("/usr/include/hurd/", "libmachdev/*")
    pisitools.dodoc("BUGS", "ChangeLog", "COPYING", "README")

