# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import shelltools
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get


def install():
    def chmod(path, mode):
        shelltools.chmod("%s%s" % (get.installDIR(), path), mode)

    # Install everything
    pisitools.insinto("/", "root/*")


    # Adjust permissions
    chmod("/tmp", 01777)
    chmod("/var/tmp", 01777)
    chmod("/run/shm", 01777)
    chmod("/var/lock", 0775)
    chmod("/usr/share/baselayout/shadow", 0600)
    
        
    # /usr/lib migration
    pisitools.removeDir("/lib") 
    pisitools.dosym("usr/lib", "lib")

    pisitools.dodir("/usr/share/locale")
    pisitools.dodir("/usr/lib/locale")
    
    #hurd hacks
    pisitools.dodir("/hurd")
    pisitools.dodir("/servers/")
    pisitools.dodir("/servers/socket")
  #  shelltools.touch("exec")
   # pisitools.insinto("/servers", "exec")


    if get.ARCH() == "x86_64":
        # Directories for 32bit libraries
        pisitools.dosym("usr/lib", "lib64")   
        pisitools.dosym("lib", "usr/lib64") 

        pisitools.dodir("/lib32")
        pisitools.dodir("/usr/lib32")


    pisitools.dosym("pisilinux-release", "/etc/system-release")

    shelltools.touch("hostname")

    if get.ARCH() == "x86_64":
        shelltools.echo("hostname", "Cpisi")

    elif get.ARCH() == 'armv7h':
        shelltools.echo("hostname", "PisiArm")

    elif get.ARCH() == 'i686':
        shelltools.echo("hostname", "MisiHURD")

    pisitools.insinto("/etc", "hostname")
    pisitools.remove("/etc/inittab")


  #  for i in ["exec","crash-kill","default-pager","password","socket","startup","proc","auth","symlink"]:
   #     shelltools.touch(i)
    #    pisitools.insinto("/servers",i)


