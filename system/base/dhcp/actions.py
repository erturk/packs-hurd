#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import get

def setup():
   # shelltools.export("CFLAGS", "%s -D_GNU_SOURCE -fPIC" % get.CFLAGS())
    
    shelltools.export("DO_LPF", "1")

    autotools.autoreconf("-fi")
    autotools.configure("--with-srv-lease-file=/var/lib/dhcpd/dhcpd.leases \
                         --with-srv6-lease-file=/var/lib/dhcpd/dhcpd6.leases \
                         --with-cli-lease-file=/var/lib/dhclient/dhclient.leases \
                         --with-cli6-lease-file=/var/lib/dhclient/dhclient6.leases \
                         --enable-use-sockets")

def build():
    autotools.make("-j1")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    # Remove files we don't want
    pisitools.remove("/etc/dhcpd.conf.example")
    pisitools.remove("/etc/dhclient.conf.example")

    #pisitools.dodir("/etc/dhcp/dhclient.d")

    # Create directory hierarchy in /var
    pisitools.dodir("/var/lib/dhcpd")
    pisitools.dodir("/var/lib/dhclient")

    # Sample configuration files
    pisitools.insinto("/usr/share/doc/dhcp", "client/dhclient.conf.5", "dhclient.conf.exsample")
    pisitools.insinto("/usr/share/doc/dhcp", "server/dhcpd.conf.5", "dhcpd.conf.example")
    pisitools.insinto("/usr/share/doc/dhcp", "doc/examples/dhclient-dhcpv6.conf")
    pisitools.insinto("/usr/share/doc/dhcp", "doc/examples/dhcpd-dhcpv6.conf")

    pisitools.dodoc("LICENSE", "README", "RELNOTES")
