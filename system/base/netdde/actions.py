#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools


def build():
    autotools.make("convert")
    autotools.make()
    shelltools.system("find dde -name \*.o | sed -e 's_^dde/__' > driver_list")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

