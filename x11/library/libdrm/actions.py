# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

def setup():
    pisitools.dosed("configure.ac", "pthread-stubs", deleteLine=True)
   

   
    pisitools.cflags.add(" -I/usr/include/mach/machine")    
    autotools.autoreconf("-vfi") 

   
    options = " --prefix=/usr --disable-vmwgfx --disable-radeon --disable-intel --disable-nouveau --disable-amdgpu     --disable-vmwgfx-experimental-api \
		--disable-nouveau-experimental-api --disable-libkms"
		
    if get.ARCH() == 'armv7h':

	options += " --enable-omap-experimental-api --enable-exynos-experimental-api --enable-tegra-experimental-api"
	
    autotools.configure(options)
 


def build():
    autotools.make('CFLAGS="%s -I/usr/include/mach/machine"'% get.CFLAGS())

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    pisitools.dodoc("README")
